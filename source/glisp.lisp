;;
;; Copyright© 2020 Genworks International and David J Cooper Jr. 
;;
;; This source file is part of the General-purpose Declarative
;; Language project (GDL).
;;
;; This source file contains free software: you can redistribute it
;; and/or modify it under the terms of the GNU Affero General Public
;; License as published by the Free Software Foundation, either
;; version 3 of the License, or (at your option) any later version.
;; 
;; This source file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;; 
;; You should have received a copy of the GNU Affero General Public
;; License along with this source file.  If not, see
;; <http://www.gnu.org/licenses/>.
;; 

(in-package :glisp)

;;
;; 4 forms below merged into devo 2020-08-17
;;

(defun getuid ()
  #-(or allegro ccl) (error "need getuid impl.")
  #+(or allegro ccl)
  (#+allegro excl.osi:getuid #+ccl ccl::getuid))

(defun setuid (num)
    #-(or allegro ccl) (error "need setuid impl.")
    (#+allegro excl.osi:setuid #+ccl ccl::setuid num))

(defun setgid (num)
    #-(or allegro ccl) (error "need setgid impl.")
    (#+allegro excl.osi:setgid #+ccl ccl::setgid num))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (export 'getuid :glisp) (export 'setuid :glisp) (export 'setgid :glisp))


;;
;; Merged into devo 2020-08-17
;;

#-(or allegro lispworks cmu sbcl ccl abcl ecl clasp clisp) 
(error "Need implementation for executable-homedir-pathname for currently running lisp.~%")
(#+allegro
 excl:without-package-locks #-allegro progn
 (defun executable-homedir-pathname ()
   #+allegro (translate-logical-pathname "sys:")
   #+sbcl (make-pathname :name nil :type nil :defaults sb-ext:*core-pathname*)
   #+(or lispworks ccl ecl clisp)
   (let* ((exe (first (glisp:basic-command-line-arguments)))
	  (exe-dir (pathname-directory exe)))
     (if (eql (first exe-dir) :relative)
	 (probe-file
	  (merge-pathnames (make-pathname :directory exe-dir :defaults nil)
			   (uiop/os:getcwd)))
	 (make-pathname :name nil :type nil :defaults exe)))
   #+clasp (core:argv 0)
   #+abcl
   (warn "Don't know how to get executable-homedir-pathname on ~a! Please find out.~%"
	 (lisp-implementation-type))))


;;
;; Merged into devo 2020-08-17.
;;

(#+allegro
 excl:without-package-locks #-allegro progn
 (#+allegro
  excl:without-redefinition-warnings #-allegro progn
  (defun process-run-function (name-or-options preset-function &rest initial-bindings)
    (bt:make-thread preset-function :name name-or-options :initial-bindings initial-bindings))))
