;;
;; Copyright© 2020 Genworks International and David J Cooper Jr. 
;;
;; This source file is part of the General-purpose Declarative
;; Language project (GDL).
;;
;; This source file contains free software: you can redistribute it
;; and/or modify it under the terms of the GNU Affero General Public
;; License as published by the Free Software Foundation, either
;; version 3 of the License, or (at your option) any later version.
;; 
;; This source file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;; 
;; You should have received a copy of the GNU Affero General Public
;; License along with this source file.  If not, see
;; <http://www.gnu.org/licenses/>.
;; 


(in-package :gwl)

;;
;; Merged into devo 2020-08-14
;;
(#+allegro
 excl:without-package-locks
 #-allegro progn
 (defun publish-gwl-app (path string-or-symbol &key publish-args make-object-args (server *wserver*))
	     "Void. Publishes an application, optionally with some initial arguments to be passed in as input-slots.

:arguments (path \"String. The URL pathname component to be published.\"
            string-or-symbol \"String or symbol. The object type to insantiate.\")

:&key (make-object-args \"Plist. Extra arguments to pass to make-object.\")
"

	     (apply #'publish
		    :path path
		    :server server
		    :function #'(lambda(req ent)
				  (gwl-make-object req ent 
						   (format nil (if (stringp string-or-symbol) "~a" "~s")
							   string-or-symbol)
						   :make-object-args make-object-args))
		    publish-args)))






;;
;; Merged into devo 2020-08-17
;; 
(#+allegro
 excl:without-package-locks
 #-allegro
 progn
 (defun answer (req ent)
   (let ((query (request-query req)))
    
     (when *debug?* (format t "before multi-part processing, query is: ~s~%" query))
    
     (when (null query) (setq query (process-multipart-submission req)))
    
     (when gwl::*debug?* (print-variables query))
    
    
     (let* ((requestor (query-arg-to-root-path query "requestor"))
            (iid (make-keyword 
                  (rest (assoc "iid" query :test #'string-equal))))
            (hash-entry (gethash iid *instance-hash-table*))

	    ;;
	    ;; FLAG -- patch starts
	    ;;
             
            (root-object (or (first hash-entry)
                             (restore-from-snap iid)))
            (skin (third hash-entry))
             
	    ;;
	    ;; FLAG -- patch ends
	    ;;

            (recovery-object? (typep root-object 'session-recovery))
            (requestor (when (and root-object (not recovery-object?)) 
                         (the-object root-object 
                                     (follow-root-path requestor))))
            (bashee (when requestor (the-object requestor bashee)))
            (rest-plist (when (not recovery-object?)
                          (merge-plist-duplicates
                           (assoc-list-to-plist 
                            (remove-if 
                             #'(lambda(item) 
                                 (member (first item) 
                                         (list "requestor" "iid") 
                                         :test #'string-equal)) query) 
                            :case-sensitive? t))))
            (possible-nils (when (not recovery-object?) 
                             (the-object requestor possible-nils)))
            (rest-plist (when requestor 
                          (append rest-plist 
                                  (mapcan #'(lambda(key) (list key nil)) 
                                          (set-difference possible-nils
                                                          (plist-keys rest-plist))))))
            (settables (when bashee (the-object bashee %settable-slots%)))
            (rest-keys (plist-keys rest-plist))
            (query-plist (mapcan #'(lambda(key) (list key (getf rest-plist key)))
                                 (remove-if #'(lambda(key) (gethash (make-keyword-sensitive key) settables)) rest-keys))))
      
      
      
       (let ((respondent (the-object requestor respondent)))
        
         (the-object respondent root (set-remote-host! req))
      
         (unless (the-object respondent root do-security-check)
           (with-http-response (req ent)
             (setf (reply-header-slot-value req :cache-control) "no-cache")
             (setf (reply-header-slot-value req :pragma) "no-cache")
                     
             (with-http-body (req ent)
               (let ((*req* req) (*ent* ent))
                 (the-object respondent root security-check-failed write-html-sheet))))
           (return-from answer nil)))
        
      
       (when gwl::*debug?* (print-variables query 
                                            requestor 
                                            iid 
                                            hash-entry 
                                            root-object 
                                            recovery-object?
                                            requestor 
                                            bashee 
                                            rest-plist 
                                            possible-nils
                                            settables 
                                            rest-keys 
                                            query-plist))
      
       (let ((fe-processor (make-object 'form-element-processor 
					:bashee bashee 
					:query-plist query-plist)))
        
         (cond (recovery-object? (with-http-response (req ent :response *response-moved-permanently*)
                                   (setf (reply-header-slot-value req :location)
                                         (defaulting (the-object root-object recovery-url) *failed-request-url*))
                                   (setf (reply-header-slot-value req :cache-control) "no-cache")
                                   (setf (reply-header-slot-value req :pragma) "no-cache")
                                   (with-http-body (req ent))))
               ((null requestor) (net.aserve::failed-request req))
               (t (let ((*query-plist* query-plist) (*field-plist* rest-plist)) 
                    (the-object requestor (before-set!)))
              
		  ;;(when *debug?* (print-variables *query-plist* rest-plist rest-keys))

                  (when *debug?* (setq *f-e-p* fe-processor))
                 
                  (dolist (key rest-keys)
                    (when (gethash (make-keyword-sensitive key) settables)
		      ;;(when *debug?* (print-variables key))
                      (the-object bashee (set-slot-if-needed! (make-keyword-sensitive key) (getf rest-plist key)))))
                 
                  (the-object fe-processor validate-and-set!)
                 
                  (when 
                      (and (fboundp 'process-graphics-fields)
                           (typep (the-object requestor) (read-from-string "gwl:base-html-graphics-sheet"))
                           (the-object requestor (evaluate :view-object)))
                    (setq query-plist (funcall (read-from-string "gwl::process-graphics-fields")
                                               query query-plist root-object requestor)))


                  (when (not (equalp (the-object bashee query-plist) query-plist))
                    (the-object bashee 
				(set-slot! :query-plist query-plist 
                                           :remember? (not (member :query-plist 
                                                                   (the-object bashee transitory-slots))))))
      
                  (let ((result (let ((*req* req) (*ent* ent) (*skin* skin) 
                                      (*query* query)) (the-object requestor (after-set!)))))
                   
                    (let ((respondent (if (and (consp result) (eql (first result) :go-to)) (second result)
                                          (the-object requestor respondent))))
		      ;;
		      ;; Dashboard stuff
		      ;;
		      ;;
		      ;; FLAG - use actual application-root rather than simple root.
		      ;;
                      (when (typep root-object 'session-control-mixin) (the-object root-object (set-expires-at)))
                      (the-object respondent root (set-time-last-touched!))
                     
                      (the-object respondent root (set-slot! :last-visited-root-path 
                                                             (the-object respondent root-path)))
		      ;;
		      ;;
		      ;;
                   
                      (with-http-response (req ent :response *response-found*)
			(setf (reply-header-slot-value req :cache-control) "no-cache")
			(setf (reply-header-slot-value req :pragma) "no-cache")
                     
			(setf (reply-header-slot-value req :location) (the-object respondent url))
                     
			(let ((keys (plist-keys (the-object respondent header-plist)))
                              (values (plist-values (the-object respondent header-plist))))
                          (mapc #'(lambda(key val)
                                    (setf (reply-header-slot-value req key) val)) keys values))

			(with-http-body (req ent)
                          (let ((*req* req) (*ent* ent) (*skin* skin))
                            (multiple-value-bind (check error)
				(when (the-object respondent check-sanity?)
                                  (ignore-errors (the-object respondent check-sanity)))
                              (declare (ignore check))
                              (if error (the-object respondent (sanity-error error))))))))))))))))

;;
;; FLAG -- Then Tweaked.
;;
;; Merged into devo 2020-08-17.
;;
(#+allegro
 excl:without-package-locks #-allegro progn
 (#+allegro
  excl:without-redefinition-warnings #-allegro progn
  (defun start-session-reaper (&key (minutes 20) (debug? t) (listeners 20) extra-functions restart-server?)
    ;;(declare (ignore listeners))
    (format t "~2%Lauching Expired Session Reaper to awaken and run every ~a minute~:p~2%" minutes)
    (glisp:process-run-function
     "GWL Session Reaper" 
     #'(lambda() 
	 (do ()(nil) (sleep (* minutes 60))
	   (when debug? (format t "~&Reaper waking up...~%"))
	   (when *reap-expired-sessions?* 
	     (maphash #'(lambda(key val) (declare (ignore key))
			       (when (typep (first val) 'session-control-mixin)
				 (the-object (first val) (clear-expired-session :debug? debug?))))
		      *instance-hash-table*))
	   (when restart-server?
	     (glisp:w-o-interrupts
	       (let ((port (server-port)))
		 (when (and port (>= port 1000))
		   (net.aserve:shutdown) (net.aserve:start :port port :listeners listeners)))))
	   (glisp:w-o-interrupts
	     (mapc #'funcall (ensure-list extra-functions)) (glisp:gc-full))))))))


;;
;; Merged into devo 2020-08-17.
;;
(#+allegro
 excl:without-package-locks #-allegro progn
 (#+allegro
  excl:without-redefinition-warnings #-allegro progn
  (define-object-amendment base-html-sheet ()
    :input-slots ((instance-id nil :defaulting :settable)))))


#+nil
;;
;; FLAG -- we think the one in codbase in ajax.lisp is more complete than this. 
;;
(defun restore-from-snap (iid object)
  (let ((snap-file 
	 (merge-pathnames 
	  (make-pathname :name (format nil "~a" iid) 
			 :type "snap") (glisp:snap-folder))))
    (unless (probe-file snap-file) (error "~a not found~%" snap-file))

    (read-snapshot :filename snap-file
		   :object object
		   :keys-to-ignore (list :time-last-touched 
					 :time-instantiated 
					 :expires-at))

    (theo object (set-slot! :instance-id iid))
        
    (setf (gethash (make-keyword-sensitive (the-object object instance-id))
		       *instance-hash-table*)
	  (list object nil))))



;;
;; Merged into devo 2020-08-18
;;
(defun gwl-make-object (req ent part &key make-object-args share? skin (instance-id (when share? "share")))
  "Void. Used within the context of the body of a :function argument to Allegroserve's 
publish function, makes an instance of the specified part and responds to the request 
with a redirect to a URI representing the instance.

:arguments (req \"Allegroserve request object, as used in the function of a publish\"
            ent \"Allegroserve entity object, as used in the function of a publish\"
            package-and-part \"String. Should name the colon- (or double-colon)-separated 
package-qualified object name\")

:&key ((make-object-args nil) \"Plist of keys and values. These are passed to the object upon instantiation.\"
       (share? nil) \"Boolean. If non-nil, the instance ID will be the constant string ``share'' rather than a real instance id.\")

:example <pre>
  (publish :path \"/calendar\"
           :function #'(lambda(req ent) (gwl-make-object req ent \"calendar:assembly\")))
  </pre>"

  (unless instance-id (setq instance-id (make-new-instance-id)))


  (let ((query (request-query req)))
    (let ((part (or part (rest (assoc "part" query :test #'string-equal)))))
      (let* ((current (gethash (make-keyword-sensitive instance-id) *instance-hash-table*))
             (skin (if skin (make-instance skin) t))
             (root-part-and-version 
              (if (or (not share?) (not current))
                  (list (apply #'make-object (read-safe-string part) 
                               :instance-id instance-id :query-toplevel query 
			       make-object-args)
                        *dummy-version*)
                current)))
        (setf (gethash (first root-part-and-version) *weak-objects*) t)
        (setq root-part-and-version (append root-part-and-version (list skin)))
        (when (or (not share?) (not current)) 
          (setf (gethash (make-keyword-sensitive instance-id) *instance-hash-table*) root-part-and-version))
        
        (let ((object (first root-part-and-version)))
          (when (typep object 'session-control-mixin)
            (the-object object set-expires-at))
          (the-object object set-instantiation-time!)
          (the-object object set-time-last-touched!)
          (the-object object (set-remote-host! req :original? t)))
        
        (with-http-response (req ent :response *response-found*)
          (setf (reply-header-slot-value req :location)
            (format nil "~a" (the-object (first root-part-and-version) url)))
          (setf (reply-header-slot-value req :cache-control) "no-cache")
          (setf (reply-header-slot-value req :pragma) "no-cache")
          (with-http-body (req ent)))))))


;;
;; FLAG -- is this used anywhere? 
;;
#+nil
(defun register-instance (instance)
  (let ((iid (theo instance instance-id)))
    (let ((current (first (gethash iid gwl::*instance-hash-table*))))
      (cond ((and current (not (eql current instance)))
	     (error "Instances don't match for ~a.~%" iid))
	    (current iid)
	    (t (setf (gethash iid gwl:*instance-hash-table*) (list instance))
	       iid)))))

#+nil
(defmacro wmd (string)
  `(str
    (with-output-to-string (ss)
      (markdown:markdown ,string :stream ss))))

#+nil
(eval-when (:compile-toplevel :load-toplevel :execute)
  (export 'wmd :gwl))

;;
;; FLAG -- Tweaked.
;;
(#+allegro
 excl:without-package-locks
 #-allegro progn
  (defmacro wmd (string)
    `(with-cl-who-string()
       (str
	(with-output-to-string (ss)
	  (markdown:markdown ,string :stream ss)))))

 #+nil
 (defmacro wmd (string)
   `(str
     (with-output-to-string (ss)
       (markdown:markdown ,string :stream ss)))))

(eval-when (:compile-toplevel :load-toplevel :execute) (export 'wmd))
