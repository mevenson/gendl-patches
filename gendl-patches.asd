;;;; -*- coding: utf-8 -*-

(asdf:defsystem #:gendl-patches :description
 "The Gendl® gendl-patches Subsystem" :author
 "Genworks International" :license
 "Affero Gnu Public License (http://www.gnu.org/licenses/)" :serial t
 :version "20201012" :depends-on (:gendl :cl-markdown)
 :defsystem-depends-on nil :components
 ((:file "source/aserve") (:gendl "source/cl-lite")
  (:file "source/gdl") (:file "source/glisp") (:file "source/gwl")))
